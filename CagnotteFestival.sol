pragma solidity >=0.4.22 <0.7.0;

contract CagnotteFestival {
    mapping(address => uint) organisateurs;

    constructor() public {
        organisateurs[msg.sender] = 100;
    }

    function transferOrga(address orga, uint parts) public {
        require(organisateurs[msg.sender] - parts < organisateurs[msg.sender], "Vous ne possèdez pas les parts nécessaires!");
        organisateurs[msg.sender] -= parts;
        organisateurs[orga] = parts;
    }

    function estOrga(address orga) public view returns(bool){
        return organisateurs[orga] > 0;
    }
}
